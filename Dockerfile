FROM openjdk:8-jdk-alpine
EXPOSE 8081
RUN mkdir -p /app/
ADD build/libs/demo-0.0.1-SNAPSHOT.jar /app/demo-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/app/demo-0.0.1-SNAPSHOT.jar"]