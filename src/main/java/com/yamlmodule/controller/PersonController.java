package com.yamlmodule.controller;

import com.yamlmodule.data.Person;
import com.yamlmodule.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
public class PersonController {

    @Autowired
    PersonService personService;

    @Value("${personInfo.name}")
    private String name;

    @Value("${personInfo.birthday}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthday;

    @Value("${personInfo.programmingLanguages}")
    private List<String> programmingLanguages;

    @PostMapping(path = "/person")
    @ResponseStatus(HttpStatus.CREATED)
    public void save() {
        Person person = Person.builder().name(name).birthday(birthday).programmingLanguages(programmingLanguages).build();
        personService.save(person);
    }

}
