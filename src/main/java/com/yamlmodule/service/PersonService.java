package com.yamlmodule.service;

import com.yamlmodule.data.Person;
import com.yamlmodule.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    public void save(Person person){
        personRepository.save(person);
    }

}
