package com.yamlmodule.data;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Document
@Builder
@Data
public class Person {

    @Id
    private String id;
    private String name;
    private LocalDate birthday;
    private List<String> programmingLanguages;

}
