package com.yamlmodule.repository;

import com.yamlmodule.data.Person;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonRepository  extends MongoRepository<Person, String> {}
